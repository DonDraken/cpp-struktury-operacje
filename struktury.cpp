#include <iostream>
#include <fstream>

using namespace std;

typedef unsigned int uint;
typedef unsigned char uchar;

struct Osoba {
    string imie,nazwisko;
    //bool kobieta;
    uchar plec;
    uint id_miasto;
    string ulica;
    string nr_domu,nr_mieszkania;
    string data_urodzenia;
    string pesel;
};

struct Miasto {
    string miasto,kod,opis;
};


///należy zaimplementować
void odczytaj_miasta(string nazwa);
void zapisz_miasta(string nazwa);
void odczytaj_osoby(string nazwa);
void zapisz_osoby(string nazwa);
Miasto pobierz_miasto(int id);
Osoba pobierz_osobe(int id);
bool usun_miasto(int id);
bool usun_osobe(int id);
bool zamien_miasto(Miasto m, int id);
bool zamien_osobe(Osoba o, int id);

//przetestowac na 20 wpisach!

int main()
{
//    ofstream m("miasta.txt");
//    ofstream o("osoby.txt");
    Miasto miasta[30];
    Osoba osoby[100];
//    miasta[0].miasto="Czestochowa";
//    miasta[0].kod="42-200";
//    osoby[0].imie="Janusz";
//    osoby[0].nazwisko="Januszowski";
//    osoby[0].pesel="88888888888";
//    if (!m.is_open()) cerr << "Nie mozna zapisac miast!" << endl;
//    if (o.is_open()==false) cerr << "Nie mozna zapisac osob!" << endl;
//    m << miasta[0].miasto << ' ' << miasta[0].kod;
//    o << osoby[0].imie << ' ' << osoby[0].nazwisko
//            << ' ' << osoby[0].pesel;
//    m.close();
//    o.close();
    ifstream mi("miasta.txt");
    ifstream oi("osoby.txt");
    if (!mi.is_open()) {
        cerr << "Nie mozna zapisac miast!" << endl;
        return -1;
    }
    if (!oi.is_open()) {
        cerr << "Nie mozna zapisac osob!" << endl;
        return -2;
    }
    int mp = 0;
    while(!mi.eof()) {
        mi >> miasta[mp].miasto >> miasta[mp].kod;
        mp++;
    }
    int op=0;
    while (!oi.eof()) {
        oi >> osoby[op].imie >> osoby[op].nazwisko >> osoby[op].pesel;
        op++;
    }
    mi.close();
    oi.close();
    cout << "Osoby" << endl;
    for (int i=0;i<op;i++) {
        cout << osoby[i].imie << ' ' << osoby[i].nazwisko
             << ' ' << osoby[i].pesel << '\n';
    }
    cout << "Miasta" << endl;
    for (int i=0;i<mp;i++) {
        cout << miasta[i].miasto << ' ' <<  miasta[i].kod << '\n';
    }
    return 0;
}


